# Inventory Mobile App for GRIN-Global

Is a software tool for GRIN-Global that support cross-platform in Android and Windows-UWP.

Software purpose: Manage germplasm inventory for seeds, DNA and herbarium. 

Screenshots: [https://www.slideshare.net/edwinrojas507/grin-global-mobile-app-android-and-windows-10](https://www.slideshare.net/edwinrojas507/grin-global-mobile-app-android-and-windows-10)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

 Visual Studio 2015 Community Update 3 
 Xamarin 4.2.1.62 
 .NETv4.5
  C#
 
 Universal Windows - Target version: Windows 10 (10.0; Build 10586)
 Universal Windows - Min version: Windows 10 (10.0; Build 10240)
 
 Compiled using Android version: Android 6.0 (Marshamallow) 
 Minimum Android to target: Android 4.4 (API Level 19 - Kit Kat) 
 
### Running source code

Steps...

## Versioning

We use Git from GitLab with Visual Studio 2015 Community under Team Explorer. 

## Authors

* **Carlos Velasquez (CIP)** - *Software Developer* 
* **Edwin Rojas (CIP)** - *GRIN-Global Administrator*

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE.md](https://gitlab.com/CIP-Development/InventoryMobileSystem/blob/master/LICENSE) file for details

