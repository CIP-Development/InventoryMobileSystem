﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using InventoryMobileSystem.Droid.Helpers;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace InventoryMobileSystem.Droid.Helpers
{
    public class FileHelper : InventoryMobileSystem.Helpers.IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return System.IO.Path.Combine(path, filename);
        }
    }
}