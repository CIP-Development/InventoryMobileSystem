﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

using InventoryMobileSystem.Helpers;
using InventoryMobileSystem.Models;

namespace InventoryMobileSystem.ViewModels
{
    public class LoginPageViewModel : TemplateViewModel
    {
        IPageDialogService _pageDialogService { get; }
        private readonly RestClient _restClient;

        private string _username;
        public string UserName
        {
            get { return _username; }
            set { SetProperty(ref _username, value); }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { SetProperty(ref _password, value); }
        }

        private string _server;
        public string Server
        {
            get { return _server; }
            set { SetProperty(ref _server, value); }
        }
        
        public LoginPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService) : base(navigationService)
        {
            _pageDialogService = pageDialogService;
            _restClient = new RestClient();

            LoginCommand = new DelegateCommand(OnLoginCommandExecuted)
                .ObservesProperty(() => UserName)
                .ObservesProperty(() => Password);
            
            ServerChangedCommand = new DelegateCommand(OnServerChangedCommandExecuted);

            //Server = "genebank.cipotato.org";
            UserName = "cvelasquez";
            Password = "password";
        }
        
        public DelegateCommand ServerChangedCommand { get; }
        private void OnServerChangedCommandExecuted()
        {
            System.Diagnostics.Debug.WriteLine("OnServerChangedCommandExecuted");
            if (!Server.Equals(Settings.Server))
                Settings.Server = Server;
        }

        public DelegateCommand LoginCommand { get; }
        private async void OnLoginCommandExecuted()
        {
            IsBusy = true;
            try
            {
                if (!string.IsNullOrEmpty(Server) && !string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
                {
                    Login token = await _restClient.Login(UserName, Password);
                    if (!string.IsNullOrEmpty(token.Token))
                    {
                        Settings.Username = UserName;
                        Settings.Token = token.Token;
                        Settings.CooperatorId = token.CooperatorId.GetValueOrDefault(-1);

                        if (Xamarin.Forms.Device.Idiom == Xamarin.Forms.TargetIdiom.Desktop)
                        {
                            await _navigationService.NavigateAsync("/NavigationPage/DashboardPage");
                        }
                        else {
                            await _navigationService.NavigateAsync("/MainPage/NavigationPage/DashboardPage");
                        }
                    }
                    else
                    {
                        await _pageDialogService.DisplayAlertAsync("Login Error", token.Error, "OK");
                    }
                }
                else
                {
                    await _pageDialogService.DisplayAlertAsync("Login Error", "Server, Username and Password can not be empty", "OK");
                }
            }
            catch (Exception e) {
                await _pageDialogService.DisplayAlertAsync("System Error", e.Message, "OK");
            }
            IsBusy = false;
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            IsBusy = true;
            try
            {
                Server = Settings.Server;
                //OnLoginCommandExecuted();
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            IsBusy = false;
        }
        public override void OnNavigatedFrom(NavigationParameters parameters)
        {
            System.Diagnostics.Debug.WriteLine("OnNavigatedFrom LoginPageViewModel");
        }
    }
}
