﻿using InventoryMobileSystem.Helpers;
using InventoryMobileSystem.Interfaces;
using InventoryMobileSystem.Models;
using InventoryMobileSystem.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryMobileSystem.ViewModels
{
    public class InventoryPageViewModel : TemplateViewModel
    {
        IPageDialogService _pageDialogService { get; }
        private readonly RestClient _restClient;
        IRestService _restService { get; }

        #region properties

        private Inventory _inventory;
        public Inventory Inventory
        {
            get { return _inventory; }
            set { SetProperty(ref _inventory, value); }
        }

        private string _accessionNumber;
        public string AccessionNumber
        {
            get { return _accessionNumber; }
            set { SetProperty(ref _accessionNumber, value); }
        }
        private string _accessionName;
        public string AccessionName
        {
            get { return _accessionName; }
            set { SetProperty(ref _accessionName, value); }
        }

        #endregion

        public InventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService)
        {
            _pageDialogService = pageDialogService;
            _restClient = new RestClient();
            _restService = new RestService();

            Title = "Inventory Details";
            SaveCommand = new DelegateCommand(OnSaveCommandExecuted);
            RegisterTransactionCommand = new DelegateCommand(OnRegisterTransactionCommandExecuted);
        }
        public DelegateCommand SaveCommand { get; }
        public DelegateCommand RegisterTransactionCommand { get; }

        private async void OnSaveCommandExecuted()
        {
            try
            {
                /*if (AccessionThumbnail == null)
                {
                    await _pageDialogService.DisplayAlertAsync("Message", "Accession is empty", "OK");
                    return;
                }*/

                string result = await _restService.UpdateInventoryAsync(Inventory);
                await _pageDialogService.DisplayAlertAsync("Message", "Successfully saved", "OK");

                List<InventoryThumbnail> updateInventoryList = await _restClient.Search("@inventory.inventory_id = " + Inventory.inventory_id, "get_mob_inventory", "inventory");
                InventoryThumbnail updateInventory = updateInventoryList[0];

                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryThumbnail", updateInventory);
                await _navigationService.GoBackAsync(navigationParams);
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        private async void OnRegisterTransactionCommandExecuted()
        {
            await _navigationService.NavigateAsync("MovementPage");
        }

        public override async void OnNavigatingTo(NavigationParameters parameters)
        {
            try
            {
                InventoryThumbnail tempInventory;
                if (parameters.ContainsKey("inventory"))
                {
                    tempInventory = (InventoryThumbnail)parameters["inventory"];
                    AccessionNumber = tempInventory.AccessionNumber;
                    AccessionName = tempInventory.acc_name_cul;

                    Inventory = await _restClient.ReadInventory(tempInventory.inventory_id);
                }
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
