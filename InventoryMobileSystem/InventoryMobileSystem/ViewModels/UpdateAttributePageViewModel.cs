﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryMobileSystem.ViewModels
{
    public class UpdateAttributePageViewModel : TemplateViewModel
    {
        public UpdateAttributePageViewModel(INavigationService navigationService)
            : base(navigationService)
        {
            Title = "Update Attribute";

            SaveCommand = new DelegateCommand(OnSaveCommandExecuted);
        }

        public DelegateCommand SaveCommand { get; }

        private async void OnSaveCommandExecuted()
        {
            var navigationParams = new NavigationParameters();

            await _navigationService.GoBackAsync(navigationParams);
        }
    }
}
