﻿using InventoryMobileSystem.Helpers;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryMobileSystem.ViewModels
{
    public class MainPageViewModel : TemplateViewModel
    {
        private List<MenuItem> _menuList;
        public List<MenuItem> MenuList
        {
            get { return _menuList; }
            set { SetProperty(ref _menuList, value); }
        }

        private MenuItem _selectedMenu;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenu; }
            set { SetProperty(ref _selectedMenu, value); }
        }

        private string _userName;
        public string UserName
        {
            get { return _userName; }
            set { SetProperty(ref _userName, value); }
        }

        public MainPageViewModel(INavigationService navigationService) : base(navigationService)
        {
            _menuList = new List<MenuItem> { new MenuItem { Text = "Inventories", Path="/MainPage/NavigationPage/InventoriesPage/BackPage" },
                new MenuItem { Text = "Movements", Path = ""/*"NavigationPage/MovementsPage"*/ },
                new MenuItem { Text = "Viability Tests", Path = "NavigationPage/ViabilityTestsPage" },
                new MenuItem { Text = "Settings", Path = "NavigationPage/SettingsPage" },
                new MenuItem { Text = "Dashboard", Path = "NavigationPage/DashboardPage"} };

            NavigateCommand = new DelegateCommand<string>(OnNavigateCommandExecuted);
            LogoutCommand = new DelegateCommand(OnLogoutCommandExecuted);

            ItemTappedCommand = new DelegateCommand(OnItemTappedCommandExecuted);

            UserName = Settings.Username;
        }
        public DelegateCommand ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted()
        {
            if (!string.IsNullOrEmpty(SelectedMenuItem.Path))
            {
                await _navigationService.NavigateAsync(SelectedMenuItem.Path, null);
            }
        }

        public DelegateCommand<string> NavigateCommand { get; }
        private async void OnNavigateCommandExecuted(string path)
        {
            await _navigationService.NavigateAsync(path);
        }

        public override void OnNavigatedTo(NavigationParameters parameters)
        {
            if (parameters.ContainsKey("Username"))
            {
                UserName = (string)parameters["Username"];
            }
        }

        public DelegateCommand LogoutCommand { get; }
        private async void OnLogoutCommandExecuted()
        {
            await _navigationService.NavigateAsync("/LoginPage");
        }

        public class MenuItem {
            public string Text { get; set; }
            public string Path { get; set; }
        }
    }
}
