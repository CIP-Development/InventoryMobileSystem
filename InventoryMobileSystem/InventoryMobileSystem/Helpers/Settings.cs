// Helpers/Settings.cs
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace InventoryMobileSystem.Helpers
{
    /// <summary>
    /// This is the Settings static class that can be used in your Core solution or in any
    /// of your client applications. All settings are laid out the same exact way with getters
    /// and setters. 
    /// </summary>
    public static class Settings
    {
        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        #region Setting Constants

        private const string SettingsKey = "settings_key";
        private static readonly string StringDefault = string.Empty;


        private const string UsernameKey = "user_name_key";
        private const string CooperatorKey = "cooperator_id_key";
        private const string TokenKey = "token_key";

        #endregion
        
        public static string GeneralSettings
        {
            get { return AppSettings.GetValueOrDefault(SettingsKey, StringDefault); }
            set { AppSettings.AddOrUpdateValue(SettingsKey, value); }
        }

        public static string Username
        {
            get { return AppSettings.GetValueOrDefault(UsernameKey, StringDefault); }
            set { AppSettings.AddOrUpdateValue(UsernameKey, value); }
        }

        public static int CooperatorId
        {
            get { return AppSettings.GetValueOrDefault(CooperatorKey, -1); }
            set { AppSettings.AddOrUpdateValue(CooperatorKey, value); }
        }

        public static string Token
        {
            get { return AppSettings.GetValueOrDefault(TokenKey, StringDefault); }
            set { AppSettings.AddOrUpdateValue(TokenKey, value); }
        }

        public static int CooperatorGroupIndex
        {
            get { return AppSettings.GetValueOrDefault("CooperatorGroupIndex", -1); }
            set { AppSettings.AddOrUpdateValue("CooperatorGroupIndex", value); }
        }

        public static string Location1
        {
            get { return AppSettings.GetValueOrDefault("Location1", StringDefault); }
            set { AppSettings.AddOrUpdateValue("Location1", value); }
        }

        public static string Server
        {
            get { return AppSettings.GetValueOrDefault("Server", "genebank.cipotato.org"); }
            set { AppSettings.AddOrUpdateValue("Server", value); }
        }
        public static int InventoryMaintPolicyId
        {
            get { return AppSettings.GetValueOrDefault("InventoryMaintPolicyId", -1); }
            set { AppSettings.AddOrUpdateValue("InventoryMaintPolicyId", value); }
        }

        public static string Filter
        {
            get { return AppSettings.GetValueOrDefault("Filter", StringDefault); }
            set { AppSettings.AddOrUpdateValue("Filter", value); }
        }
        public static string SearchText
        {
            get { return AppSettings.GetValueOrDefault("SearchText", StringDefault); }
            set { AppSettings.AddOrUpdateValue("SearchText", value); }
        }



        public static string SearchAccessionSearchText
        {
            get { return AppSettings.GetValueOrDefault("SearchAccession_SearchText", StringDefault); }
            set { AppSettings.AddOrUpdateValue("SearchAccession_SearchText", value); }
        }
        public static string SearchAccessionFilter
        {
            get { return AppSettings.GetValueOrDefault("SearchAccession_Filter", StringDefault); }
            set { AppSettings.AddOrUpdateValue("SearchAccession_Filter", value); }
        }

        #region PrinterPage
        public static int PrinterIndex
        {
            get { return AppSettings.GetValueOrDefault("PrinterIndex", -1); }
            set { AppSettings.AddOrUpdateValue("PrinterIndex", value); }
        }
        #endregion
    }
}