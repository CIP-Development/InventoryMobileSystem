﻿using Prism.Unity;
using InventoryMobileSystem.Views;
using Xamarin.Forms;

namespace InventoryMobileSystem
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();
            
            NavigationService.NavigateAsync("LoginPage");
            
        }

        protected override void RegisterTypes()
        {
            Container.RegisterTypeForNavigation<NavigationPage>();
            Container.RegisterTypeForNavigation<MainPage>();
            Container.RegisterTypeForNavigation<DashboardPage>();
            //Container.RegisterTypeForNavigation<InventoriesPage>();
            Container.RegisterTypeForNavigation<LoginPage>();
            Container.RegisterTypeForNavigation<SearchInventoriesPage>();
            Container.RegisterTypeForNavigation<InventoryPage>();
            Container.RegisterTypeForNavigation<PrintingPage>();
            Container.RegisterTypeForNavigation<NewInventoryPage>();
            Container.RegisterTypeForNavigation<SearchAccessionPage>();
            Container.RegisterTypeForNavigation<MovementPage>();
            Container.RegisterTypeForNavigation<UpdateAttributePage>();
            Container.RegisterTypeForNavigation<SettingsPage>();
            Container.RegisterTypeForNavigation<BackPage>();
            Container.RegisterTypeForNavigation<ScanReaderPage>();
            Container.RegisterTypeForNavigation<ChangeLocationPage>();
            Container.RegisterTypeForNavigation<ViabilityTestsPage>();

            // Container.RegisterInstance( typeof(Helpers.RestClient), "IRestClient", new Helpers.RestClient(), new Microsoft.Practices.Unity.ContainerControlledLifetimeManager());
            Container.RegisterTypeForNavigation<ViabilityTestPage>();
            Container.RegisterTypeForNavigation<ViabilityTestsPage>();
            Container.RegisterTypeForNavigation<ViabilityTestDataPage>();

            Container.RegisterTypeForNavigationOnIdiom<InventoriesPage, ViewModels.InventoriesPageViewModel>("InventoriesPage"
                , typeof(DesktopInventoriesPage), typeof(InventoriesPage), typeof(InventoriesPage));
        }
    }
}
