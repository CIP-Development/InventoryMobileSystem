﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace InventoryMobileSystem.Models
{
    public class WrappedSelection<T> : BindableBase //INotifyPropertyChanged
    {
        private T _item;
        public T Item
        {
            get { return _item; }
            set { SetProperty(ref _item, value); }
        }


        private bool _isSelected = false;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { SetProperty(ref _isSelected, value, () => RaisePropertyChanged(nameof(SelectedColor))); }
        }

        public Color SelectedColor
        {
            get
            {
                if (IsSelected)
                {
                    //return Color.FromRgb(205, 220, 57);
                    return Color.FromHex("#eeeeee");
                }
                else
                    return Color.Transparent;
            }
        }
        
    }
}



        /*
        private T _item;
        public T Item
        {
            get { return _item; }
            set
            {
                _item = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(Item)));
            }
        }
        */

/*
    bool isSelected = false;
    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            //if (isSelected != value)
            //{
                isSelected = value;
                PropertyChanged(this, new PropertyChangedEventArgs(nameof(IsSelected)));
            //}
        }
    }
    */


    //public event PropertyChangedEventHandler PropertyChanged = delegate { };