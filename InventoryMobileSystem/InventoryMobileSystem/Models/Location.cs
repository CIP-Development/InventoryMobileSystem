﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryMobileSystem.Models
{
    public class Location
    {
        public string storage_location_part1 { get; set; }
        public string storage_location_part2 { get; set; }
        public string storage_location_part3 { get; set; }
        public string storage_location_part4 { get; set; }
    }
}
