﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryMobileSystem.Models
{
    public class CooperatorGroup
    {
        public int cooperator_id { get; set; }
        public string group_tag { get; set; }
        public int inventory_maint_policy_id { get; set; }
        public string title { get; set; }
        public int sys_group_id { get; set; }
    }
}
