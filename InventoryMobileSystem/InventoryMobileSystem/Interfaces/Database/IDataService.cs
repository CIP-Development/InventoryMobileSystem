﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using InventoryMobileSystem.Models.Database;

namespace InventoryMobileSystem.Interfaces.Database
{
    public interface IDataService
    {
        Task CheckInMember(AccessionLookup rsvp);
        Task CheckOutMember(string eventId, string userId);
        Task<bool> IsCheckedIn(string eventId, string userId, string eventName, string groupId, string groupName, long eventDate);

        Task AddNewMember(NewInventory member);
        Task<IEnumerable<NewInventory>> GetNewMembers(string eventId);
        Task RemoveNewMember(int id);
        Task<IEnumerable<NewInventory>> GetNewMembersForGroup(string groupId);
        Task<IEnumerable<AccessionLookup>> GetRSVPsForGroup(string groupId);
    }
}
