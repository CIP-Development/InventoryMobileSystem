﻿using Xamarin.Forms;

namespace InventoryMobileSystem.Views
{
    public partial class SearchInventoriesPage : ContentPage
    {
        public SearchInventoriesPage()
        {
            InitializeComponent();

            
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ViewModels.SearchInventoriesPageViewModel>(this, "Focus", (sender) => {
                //editorList.Focus();
                //searchBarText.Focus();
            });
        }
    }
}
