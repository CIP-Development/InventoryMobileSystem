﻿using Xamarin.Forms;

namespace InventoryMobileSystem.Views
{
    public partial class BackPage : ContentPage
    {
        public BackPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            MessagingCenter.Send<BackPage>(this, "goback");
            Navigation.PopAsync();
        }
    }
}
